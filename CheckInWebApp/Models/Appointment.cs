﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckInWebApp.Models
{
    public class Appointment
    {
        public int Id { get; set; }
        
        //Foreign Keys
        public int PatientId { get; set; }
        public int DoctorId { get; set; }

        //Navigation property
        public Patient Patient { get; set; }
        public Doctor Doctor { get; set; }

        public DateTime DateTime { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }
}