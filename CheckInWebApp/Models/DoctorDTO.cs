using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckInWebApp.Models
{
    public class DoctorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string AccountSID { get; set; }
        public string AuthToken { get; set; }
        public string TwilioPhone { get; set; }
    }
}