﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckInWebApp.Models
{
    public class AppointmentDTO
    {
        public int Id { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public string Status { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string Message { get; set; }
    }
}