var ViewModel = function () {
    var self = this;
    self.appointments = ko.observableArray();
    self.error = ko.observable();
    self.doctors = ko.observableArray();
    self.doctorDetail = ko.observable();
    self.patients = ko.observableArray();
    self.date = ko.observable();

    //Get Current Date to display
    self.date(new Date().toDateString());

    //URI Variables

    var appointmentsUri = 'api/appointments/';
    var doctorsUri = '/api/doctors/';
    var patientsUri = '/api/patients';

    function ajaxHelper(uri, method, data, data2) {
        self.error(''); //clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null,
            data2: data2 ? JSON.stringify(data2) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    //Retrieve Functions
    //Retrieve All Valid Appointments
    function getAllAppointments() {
        ajaxHelper(appointmentsUri, 'GET').done(function (data) {
            
            for (var i = 0; i < data.length; i++) {
                var hour = data[i].Hour;
                data[i].Hour = hour > 12 ? hour - 12 : hour == 0 ? hour = 12 :  hour;
                var ampm = hour > 12 ? " pm" :  " am";
                var minute = data[i].Minute;
                data[i].Minute = minute < 10 ? "0" + minute + ampm: minute + ampm;
            }
            
            self.appointments(data);
        });
    }

    //Retrieve Single Appointment
    function getAppointment(appointmentId) {
        ajaxHelper(appointmentsUri + appointmentId, 'GET').done(function (data) {
            return data;
        });
    }

    //Retrieve all Patients
    function getPatients() {
        ajaxHelper(patientsUri, 'GET').done(function (data) {
            self.patients(data);
        });
    }

    //Retrieve all Doctors
    function getDoctors() {
        ajaxHelper(doctorsUri, 'GET').done(function (data) {
            self.doctors(data);
        });
    }

    //Retrieve Doctor Detail
    self.getDoctorDetail = function (item) {
        ajaxHelper(doctorsUri + item.DoctorId, 'GET').done(function (data) {
            self.doctorDetail(data);
        });
    }

    //Add Functions
    //Add Appointment

    self.newAppointment = {
        Patient: ko.observable(),
        Doctor: ko.observable(),
        DateTime: ko.observable(),
        Status: ko.observable()
    }

    self.addAppointment = function (formElement) {
        var appointment = {
            PatientId: self.newAppointment.Patient().Id,
            DoctorId: self.newAppointment.Doctor().Id,
            DateTime: self.newAppointment.DateTime(),
            Status: self.newAppointment.Status()
        };

        ajaxHelper(appointmentsUri, 'POST', appointment).done(function (item) {
            self.appointments.push(item);
        });
    }

    //Add Doctor

    self.newDoctor = {
        Name: ko.observable(),
        Phone: ko.observable(),
        AccountSID: ko.observable(),
        AuthToken: ko.observable(),
        TwilioPhone: ko.observable()
    }

    self.addDoctor = function (formElement) {
        var doctor = {
            Name: self.newDoctor.Name,
            Phone: self.newDoctor.Phone,
            AccountSID: self.newDoctor.AccountSID,
            AuthToken: self.newDoctor.AuthToken,
            TwilioPhone: self.newDoctor.TwilioPhone
        };

        ajaxHelper(doctorsUri, 'POST', doctor).done(function (item) {
            self.appointments.push(item);
        });

    }

    //Check in and send text
    //If text is sent update appointment data

    self.checkIn = ko.observable();
    //
    //I need to send 
    self.putAppointment = function (item) {
        item.Message = prompt("Would you like to send a message?", "Message");
        ajaxHelper(appointmentsUri + item.Id, 'PUT', item).done(function (data) {
            self.checkIn(data);
        });
    }

    self.putDoctor = function (item) {
        var doctor = {
            Id: self.doctorDetail().Id,
            Name: self.doctorDetail().Name,
            Phone: self.doctorDetail().Phone,
        AccountSID: self.doctorDetail().AccountSID,
        AuthToken: self.doctorDetail().AuthToken,
        TwilioPhone: self.doctorDetail().TwilioPhone
        };
        ajaxHelper(doctorsUri + doctor.Id, 'PUT', doctor)
    }

    //Fetch Appointments Data
    getAllAppointments();
    getDoctors();
    getPatients();

    var refresh = setInterval(function () { getAllAppointments() }, 15000)

};

ko.applyBindings(new ViewModel());
