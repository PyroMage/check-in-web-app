namespace CheckInWebApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CheckInWebApp.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<CheckInWebApp.Models.CheckInWebAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CheckInWebApp.Models.CheckInWebAppContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            
            context.Patients.AddOrUpdate(x => x.Id,
                new Patient() { Id = 1, Name = "Wally West" },
                new Patient() { Id = 2, Name = "Claire Bennett" },
                new Patient() { Id = 3, Name = "Peter Parker" }
                );
            
            context.Doctors.AddOrUpdate(x => x.Id,
                new Doctor() { Id = 1, Name = "Stephen Strange", Phone = "16172814416", AccountSID = "ACe73926084ef4fbd20a5ff9b22a443455", AuthToken = "1b9398562649d51576ed632eb4d73164", TwilioPhone = "16173408851"},
                new Doctor() { Id = 2, Name = "Otto Octavius", Phone = "16172814416", AccountSID = "ACe73926084ef4fbd20a5ff9b22a443455", AuthToken = "1b9398562649d51576ed632eb4d73164", TwilioPhone = "16173408851" },
                new Doctor() { Id = 3, Name = "Apollo", Phone = "16172814416", AccountSID = "ACe73926084ef4fbd20a5ff9b22a443455", AuthToken = "1b9398562649d51576ed632eb4d73164", TwilioPhone = "16173408851" }
                );
            
            context.Appointments.AddOrUpdate(x => x.Id,
                new Appointment() { Id = 1, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 20, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 2, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 20, 3, 18, 0), Status = "Upcoming" },
                new Appointment() { Id = 3, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 20, 8, 9, 0), Status = "Upcoming" },
                new Appointment() { Id = 4, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 20, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 5, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 20, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 6, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 20, 23, 47, 0), Status = "Upcoming" },
                new Appointment() { Id = 7, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 21, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 8, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 21, 3, 18, 0), Status = "Upcoming" },
                new Appointment() { Id = 9, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 21, 8, 9, 0), Status = "Upcoming" },
                new Appointment() { Id = 10, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 21, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 11, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 21, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 12, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 21, 23, 47, 0), Status = "Upcoming" },
                new Appointment() { Id = 13, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 22, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 14, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 22, 3, 18, 0), Status = "Upcoming" },
                new Appointment() { Id = 15, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 22, 8, 9, 0), Status = "Upcoming" },
                new Appointment() { Id = 16, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 22, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 17, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 22, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 18, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 22, 23, 47, 0), Status = "Upcoming" },
                new Appointment() { Id = 19, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 23, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 20, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 23, 3, 18, 0), Status = "Upcoming" },
                new Appointment() { Id = 21, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 23, 8, 9, 0), Status = "Upcoming" },
                new Appointment() { Id = 22, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 23, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 23, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 23, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 24, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 23, 23, 47, 0), Status = "Upcoming" },
                new Appointment() { Id = 25, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 24, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 26, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 24, 3, 18, 0), Status = "Upcoming" },
                new Appointment() { Id = 27, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 24, 8, 9, 0), Status = "Upcoming" },
                new Appointment() { Id = 28, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 24, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 29, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 24, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 30, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 24, 23, 47, 0), Status = "Upcoming" },
                new Appointment() { Id = 31, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 25, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 32, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 25, 3, 18, 0), Status = "Upcoming" },
                new Appointment() { Id = 33, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 25, 8, 9, 0), Status = "Upcoming" },
                new Appointment() { Id = 34, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 25, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 35, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 25, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 36, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 25, 23, 47, 0), Status = "Upcoming" },
                new Appointment() { Id = 37, PatientId = 1, DoctorId = 1, DateTime = new DateTime(2015, 09, 26, 1, 20, 0), Status = "Upcoming" },
                new Appointment() { Id = 38, PatientId = 2, DoctorId = 2, DateTime = new DateTime(2015, 09, 26, 4, 30, 0), Status = "Upcoming" },
                new Appointment() { Id = 39, PatientId = 3, DoctorId = 3, DateTime = new DateTime(2015, 09, 26, 11, 10, 0), Status = "Upcoming" },
                new Appointment() { Id = 40, PatientId = 2, DoctorId = 1, DateTime = new DateTime(2015, 09, 26, 14, 51, 0), Status = "Upcoming" },
                new Appointment() { Id = 41, PatientId = 3, DoctorId = 2, DateTime = new DateTime(2015, 09, 26, 18, 35, 0), Status = "Upcoming" },
                new Appointment() { Id = 42, PatientId = 1, DoctorId = 3, DateTime = new DateTime(2015, 09, 26, 23, 47, 0), Status = "Upcoming" }
                );
                
        }
    }
}
