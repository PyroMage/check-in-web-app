﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CheckInWebApp.Models;
using Twilio;

namespace CheckInWebApp.Controllers
{
    public class AppointmentsController : ApiController
    {
        private CheckInWebAppContext db = new CheckInWebAppContext();

        // GET: api/Appointments
        public IQueryable<AppointmentDTO> GetAppointments()
        {
            var appointments = from b in db.Appointments
                               where b.DateTime.Year == DateTime.Today.Year
                               && b.DateTime.Month == DateTime.Today.Month
                               && b.DateTime.Day == DateTime.Today.Day
                               && b.Status != "Checked In"
                        select new AppointmentDTO()
                        {
                            Id = b.Id,
                            Hour = b.DateTime.Hour,
                            Minute = b.DateTime.Minute,
                            Status = b.Status,
                            PatientId = b.Patient.Id,
                            PatientName = b.Patient.Name,
                            DoctorId = b.Doctor.Id,
                            DoctorName = b.Doctor.Name,
                            Message = b.Message
                        };
                 
            return appointments;
        }

        // GET: api/Appointments/5
        [ResponseType(typeof(Appointment))]
        public async Task<IHttpActionResult> GetAppointment(int id)
        {
            Appointment appointment = await db.Appointments.FindAsync(id);
            if (appointment == null)
            {
                return NotFound();
            }

            return Ok(appointment);
        }

        // PUT: api/Appointments/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAppointment(int id, Appointment appt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != appt.Id)
            {
                return BadRequest();
            }

            var doctor = await db.Doctors.FindAsync(appt.DoctorId);

            string doctorName = doctor.Name;
            string doctorPhone = doctor.Phone;
            string AccountSid = doctor.AccountSID;
            string AuthToken = doctor.AuthToken;
            string twilioPhone = "+" + doctor.TwilioPhone;

            var patient = await db.Patients.FindAsync(appt.PatientId);

            string patientName = patient.Name;

            var twilio = new TwilioRestClient(AccountSid, AuthToken);
            
            var emssage = twilio.SendMessage(twilioPhone, "+" + doctorPhone, "Dr. " + doctorName + ", " + patientName + " has arrived for his appointment! " + appt.Message, "");

            //TO DO: Having a datetime2 to datetime conversion issue when updating database
            // Loading record from database temporary solution
            appt = await db.Appointments.FindAsync(appt.Id);
            appt.Status = "Checked In";

            db.Entry(appt).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AppointmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Appointments
        [ResponseType(typeof(Appointment))]
        public async Task<IHttpActionResult> PostAppointment(Appointment appointment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Appointments.Add(appointment);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = appointment.Id }, appointment);
        }

        // DELETE: api/Appointments/5
        [ResponseType(typeof(Appointment))]
        public async Task<IHttpActionResult> DeleteAppointment(int id)
        {
            Appointment appointment = await db.Appointments.FindAsync(id);
            if (appointment == null)
            {
                return NotFound();
            }

            db.Appointments.Remove(appointment);
            await db.SaveChangesAsync();

            return Ok(appointment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AppointmentExists(int id)
        {
            return db.Appointments.Count(e => e.Id == id) > 0;
        }
    }
}