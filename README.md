To run the sample locally from Visual Studio:

* Build the sample.
* Open the Package Manager Console 
* In the Package Manager Console window, enter the following command: Update-Database
* Press F5 to debug.

Description of what I did:
I used C# for the server-side code and knockout.js for the client-side data binding.  The UI is very simple.  It contains a display very similar to the example in the instructions.  The data refreshes every 15 seconds to display appointments for the current day and that don't have a status of "Checked In".  When 'Check-in' is clicked a prompt window is displayed to enter an additional message.  The default text is Message (as I'm typing this I realized I should have probably kept the text blank).  If Ok is hit then the message is appended to the notification, otherwise no extra message is sent.  The send text functionality uses Twilio.  This requires a Twilio account SID, Auth token, and Phone. If the doctors name is clicked, a new area is displayed where the doctor information can be edited.  Here is where new Twilio information and recipient phone data can be changed.  

Side Notes:

* Database is seeded with 6 appointments per day until the 26th. There is functionality to add a new appointment, but it has been commented out because it created some corruption in the display.  If needed, another call to Update-Database will reseed the appointments.
* There is no data validation, so the phone numbers need to be include only digits and include the country code.  For example 16172814416.

The bonus functionality:

* Added the ability to send an extra message.
* Responsive design.  Only the forms scale down nicely.  The main display does not.
*